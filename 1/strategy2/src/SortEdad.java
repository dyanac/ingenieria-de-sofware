
public class SortEdad implements Sort{
	Alumno[] al;
	public SortEdad(Alumno[] al)
	{
		this.al = al;
	}
	public boolean compare(Alumno a1, Alumno a2)
	{
		return a1.edad < a2.edad;
	}
	public void sort()
	{
		int j;
	    boolean flag = true;
	    Alumno temp;
	    while(flag)
	    {
	    	flag = false;
	        for(j = 0; j < al.length-1; ++j)
	        {
	        	if(compare(al[j], al[j+1]))
	            {
	        		temp = al[j];
	        		al[j] = al[j+1];
	        		al[j+1] = temp;
	        		flag = true;  
	            } 
	        } 
	    }
	    print();
	}
	public void print()
	{
		int i;
		for(i = 0; i < al.length; ++i)
			System.out.println(al[i].nombre+" "+al[i].codigo+" "+al[i].edad+"\n");
	}
}
