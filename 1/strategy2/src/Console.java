import java.util.Scanner;

public class Console {
	public static void main(String[] arg)
	{
		Alumno[] al = {
				new Alumno("mario","123",13),
				new Alumno("juan","109",15),
				new Alumno("maria","103",11),
				new Alumno("rodrigo","138",14),
				new Alumno("juana","140",15),
				new Alumno("miguel","141",23),
				new Alumno("alberto","104",13),
				new Alumno("andrea","125",11)
		};
		Ordenador ord = new Ordenador();
		ord.ordenar(new SortNombre(al));
		//ord.ordenar(new SortCodigo(al));
		//ord.ordenar(new SortEdad(al));
		/*
		System.out.println("Como desea ordenar a los alumnos: 1.Nombre 2.Codigo 3.Edad \n");
		Scanner scan = new Scanner(System.in);
		String s = scan.next();
		if(s == "1")
			ord.ordenar(new SortNombre(al));
		else if(s == "2")
			ord.ordenar(new SortCodigo(al));
		else if(s == "3")
			ord.ordenar(new SortEdad(al));
		scan.close();
		*/
	}
}
