
public interface Sort {
	public void sort();
	public boolean compare(Alumno a1, Alumno a2);
	public void print();
}
