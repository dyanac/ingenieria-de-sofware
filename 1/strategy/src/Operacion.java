public interface Operacion {
	int operar(int a, int b);
}

abstract class Operacion2 {
	public abstract int operar(int a, int b);

	public void f() {
		operar(3, 5);
	}
}